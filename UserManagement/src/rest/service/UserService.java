package rest.service;
import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



import http.annotations.*;

@Singleton
@Path("/UserService")
public class UserService {

  UserDao userDao = new UserDao();
/*
   @GET
   @Path("/users")
   @Produces(MediaType.APPLICATION_JSON)
   public List<User> getUsers(){
	  
      return userDao.getAllUsers();
	
   }
   */
  
   @GET
   @Path("/users/{id}")
   @Produces(MediaType.APPLICATION_JSON)
   public User get(@PathParam("id") int id) {
	  if( userDao.idExists(id)){
       return userDao.getUser(id);
	  }else{
		return null;
	  }
   }

   @DELETE
   @Path("/users/{id}")
   @Produces(MediaType.APPLICATION_JSON)
   public Response remove(@PathParam("id") int id) {
	   
	   if( userDao.idExists(id)){
       userDao.removeUser(id);
       return Response.status(200).entity("{\"Result\":\"OK:\"}").build();
	   }else{
			return null;
		  }
   }
   
   @POST
   @Path("/users/{id}") //?
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public Response createUserInJSON(@PathParam("id") int id,User user) {
	   
	   user.setId(id);
       userDao.addUser(user);
       return Response.status(200).entity("{\"Result\":\"OK:\"}").build();
       
   }
   
   @HEAD
   @Path("/users/{id}")
   public Response getHeaders(@PathParam("id") Integer id) {
	  
	   if (userDao.idExists(id)) {
	        return Response.ok().build();
	    } else {
	        return Response.status(Response.Status.NOT_FOUND).build();
	    }
   }
   
   @PUT
   @Path("/users/{id}")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public Response updateUserById(@PathParam("id") Integer id, User user) 
   {     
       if(user.getName()== null) {
           return Response.status(400).entity("Please provide the  name !!").build();
       }
        
        userDao.removeUser(id);
        user.setId(id);
        userDao.addUser(user);
        
       return Response.ok().entity("{\"Result\":\"OK:\"}").build();
   }
 /*  
   @PATCH
   @Path("/users/{id}")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public Response changeDetail(@PathParam("id") Integer id, User user){
	   
	   if(user.getName()!=null){
		   userDao.getUser(id).setName(user.getName());
	   }
	   if(user.getSurname()!=null){
		   userDao.getUser(id).setSurname(user.getSurname());
	   }   
	   if(user.getBirthDate()!=null){
		   userDao.getUser(id).setBirthDate(user.getBirthDate());
	   }		  
	   return Response.status(200).entity("{\"Result\":\"OK:\"}").build();
	   
   }
   */
   
   
   
   @PATCH
   @Path("/users/{id}")
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public Response changeDetail(@PathParam("id") Integer id, NewData data){
	   
	   if(data.getReplace().equals("name")){
		   userDao.getUser(id).setName(data.getValue());
	   }
	   if(data.getReplace()== "surname"){
		   userDao.getUser(id).setSurname(data.getValue());
	   }
	   if(data.getReplace()== "birthDate"){
		   userDao.getUser(id).setBirthDate(data.getValue());
	   }
	   
	   return Response.status(200).entity("{\"Result\":\"OK:\"}").build();
	   
   }
 
  @OPTIONS
  @Path("/users")
  public Response getOptions (){
	    return Response.ok().header("Allow", "POST")
                .header("Allow", "PUT")
                .header("Allow", "GET")
                .header("Allow", "DELETE")
                .header("Allow", "HEAD")
                .header("Allow", "OPTIONS")
                .header("Allow", "PATCH")
                .build();
	  
  }
  
  

}
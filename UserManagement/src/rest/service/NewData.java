package rest.service;

public class NewData {
	
	private String replace;
	private String value;
	
	public NewData() {
	
	}
	
	public NewData(String replace, String value) {
		super();
		this.replace = replace;
		this.value = value;
	}
	public String getReplace() {
		return replace;
	}
	public void setReplace(String replace) {
		this.replace = replace;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	

}

package rest.service;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
	private ArrayList<User> userList;
	
	public UserDao(){
		userList = new ArrayList<>();
	    //User emptyUser = new User(0, "null", "null","null");
	    User user2 = new User(0, "Anna", "Jaskolka","28-01-91");
	    userList.add(user2);	
	}   
    
   public List<User> getAllUsers(){

      return userList;
   }
   
   public User getUser(int id){
	   User foundUser = null;
	   for(User u:userList){	   
 			if(u.getId()==id){				
 				foundUser=u;
 				break;
 			}
 		}
      return  foundUser;
   }
   
   public String addUser(User user){
       
    userList.add(user);
       return "added";
   }
   
   public String removeUser(int id){
	   ArrayList<User> tmpList=userList;
	   for(User u:tmpList){
		   
  			if(u.getId()==id){				
  				userList.remove(u);
		      break;
  			}
  		}
       return "removed";
   }
   
    boolean idExists(int id){
	   boolean exists = false;
	   		for(User u:userList){
	   			if(u.getId()==id){
	   			  exists=true;
			      break;
	   			}
	   		}
    return exists;
   }
   
  
}